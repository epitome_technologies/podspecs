Pod::Spec.new do |s|
  s.name             = 'LocalAuthenticator'
  s.version          = '0.1.3'
  s.summary          = 'Pod for easy local authentication in ios apps.'

  s.description      = <<-DESC
Pod for handling biometric and passcode local authentication in ios apps.
                       DESC

  s.homepage         = 'https://bitbucket.org/epitome_technologies/localauthenticator'
  s.license          = 'MIT'
  s.author           = { 'Arsh Aulakh' => 'arsh@epitometechnologies.com' }
  s.source           = { :git => 'https://bitbucket.org/epitome_technologies/localauthenticator.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'

  s.source_files = 'LocalAuthenticator/Classes/**/*.swift'
  
  s.frameworks = 'Foundation', 'LocalAuthentication'
end
