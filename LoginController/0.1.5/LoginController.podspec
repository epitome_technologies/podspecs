#
# Be sure to run `pod lib lint LoginController.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'LoginController'
  s.version          = '0.1.5'
  s.summary          = 'Pod containing reusable `Login` flow for novagems mobile applications.'

  s.description      = <<-DESC
Pod containing reusable `Login` flow for novagems mobile iOS applications.
                       DESC

  s.homepage         = 'https://bitbucket.org/epitome_technologies/logincontroller'
  s.license          = 'MIT'
  s.author           = { 'Arsh Aulakh' => 'arsh@epitometechnologies.com' }
  s.source           = { :git => 'https://bitbucket.org/epitome_technologies/logincontroller.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'

  s.source_files = 'LoginController/Classes/**/*.swift'
  
  s.resource_bundles = {
    'LoginController' => ['LoginController/Assets/*.xcassets', 'LoginController/Classes/**/*.xib']
  }

  s.frameworks = 'UIKit'

  s.dependency 'Epilities'
  s.dependency 'Material'
end
