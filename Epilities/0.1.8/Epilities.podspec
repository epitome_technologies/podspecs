Pod::Spec.new do |s|
  s.name             = 'Epilities'
  s.version          = '0.1.8'
  s.summary          = 'Epilities - Utilities by Epitome'

  s.description      = <<-DESC
Epilities - Swift Utilities pod by Epitome Technologies.
                       DESC

  s.homepage         = 'https://bitbucket.org/epitome_technologies/epilities'
  s.license          = 'MIT'
  s.author           = { 'Arsh Aulakh' => 'arsh@epitometechnologies.com' }
  s.source           = { :git => 'https://bitbucket.org/epitome_technologies/epilities.git', :tag => s.version.to_s }
  s.default_subspec  = 'Core'

  s.ios.deployment_target = '9.0'

  s.frameworks = 'UIKit', 'MapKit'

  s.subspec 'Core' do |core|
    core.source_files = 'Epilities/Classes/Modules/Core/**/*.swift'
    core.resources    = 'Epilities/Classes/Modules/Core/**/*.xib'
    core.resource_bundles = {
        'Epilities' => [
            'Epilities/Assets/*.*',
            'Epilities/Classes/Modules/Core/**/*.xib'
        ]
    }

    core.dependency 'DropDown'
    core.dependency 'SwiftyJSON'
    core.dependency 'SwipeCellKit'
  end

#s.subspec 'Maps' do |maps|
#maps.source_files = 'Epilities/Classes/Modules/Maps/**/*.swift'
#maps.resources    = 'Epilities/Classes/Modules/Maps/**/*.xib'

#maps.dependency 'GoogleMaps'
#maps.dependency 'GooglePlaces'
#maps.dependency 'Epilities/Core'
#end

end
